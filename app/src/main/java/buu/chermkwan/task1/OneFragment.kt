package buu.chermkwan.task1

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import buu.chermkwan.task1.databinding.FragmentOneBinding
import kotlinx.android.synthetic.main.fragment_one.*
import kotlin.random.Random

class OneFragment : Fragment() {
    private lateinit var binding:FragmentOneBinding
    private var correctScore: Int = 0
    private var incorrectScore: Int = 0
    private var progressTime:Int = 100

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentOneBinding>(inflater, R.layout.fragment_one, container, false)
        timer()
        game()
        return binding.root
    }

    private fun timer() {
        binding.apply {
            object : CountDownTimer(20000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    txtTime.setText( getString(R.string.time_remaining) + millisUntilFinished / 1000)
                    progressBar.progress = progressTime
                    progressTime -= 5
                }

                override fun onFinish() {
                    view?.findNavController()?.navigate(R.id.action_oneFragment2_to_zeroFragment2)
                }
            }.start()
        }
    }

    private fun game() {
        binding.apply {
            var randomNum1 = Random.nextInt(0, 10)
            var randomNum2 = Random.nextInt(0, 10)
            var randomOper = Random.nextInt(0, 3)
            val randomBtn = Random.nextInt(0, 3)

            txtNum1.text = randomNum1.toString()
            txtNum2.text = randomNum2.toString()

            var Answer: Int

            if (randomOper == 0) {
                Answer = randomNum1 + randomNum2
                txtOper.text = getString(R.string.plus)
            } else if (randomOper == 1) {
                Answer = randomNum1 - randomNum2
                txtOper.text = getString(R.string.positive)
            } else {
                Answer = randomNum1 * randomNum2
                txtOper.text = getString(R.string.mul)
            }

            if (randomBtn == 0) {
                btnAns1.text = Answer.toString()
                btnAns2.text = (Answer-1).toString()
                btnAns3.text = (Answer-2).toString()
            } else if (randomBtn == 1) {
                btnAns2.text = Answer.toString()
                btnAns1.text = (Answer+2).toString()
                btnAns3.text = (Answer+1).toString()
            } else {
                btnAns3.text = Answer.toString()
                btnAns1.text = (Answer-1).toString()
                btnAns2.text = (Answer+1).toString()
            }

            btnAns1.setOnClickListener {
                if (btnAns1.text.toString() == Answer.toString()) {
                    txtTorF.text = getString(R.string.correct)
                    correctScore ++
                    txtC.text = correctScore.toString()
                    game()
                } else {
                    txtTorF.text = getString(R.string.incorrect)
                    incorrectScore ++
                    txtIn.text = incorrectScore.toString()
                    game()
                }
            }
            btnAns2.setOnClickListener {
                if (btnAns2.text.toString() == Answer.toString()) {
                    txtTorF.text = getString(R.string.correct)
                    correctScore ++
                    txtC.text = correctScore.toString()
                    game()
                } else {
                    txtTorF.text = getString(R.string.incorrect)
                    incorrectScore ++
                    txtIn.text = incorrectScore.toString()
                    game()
                }
            }
            btnAns3.setOnClickListener {
                if (btnAns3.text.toString() == Answer.toString()) {
                    txtTorF.text = getString(R.string.correct)
                    correctScore ++
                    txtC.text = correctScore.toString()
                    game()
                } else {
                    txtTorF.text = getString(R.string.incorrect)
                    incorrectScore ++
                    txtIn.text = incorrectScore.toString()
                    game()
                }
            }
        }

    }

}