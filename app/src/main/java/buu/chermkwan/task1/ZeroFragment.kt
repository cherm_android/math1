package buu.chermkwan.task1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.chermkwan.task1.databinding.FragmentZeroBinding

class ZeroFragment : Fragment() {
    private lateinit var binding:FragmentZeroBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentZeroBinding>(inflater, R.layout.fragment_zero, container, false)
        binding.btnPlay.setOnClickListener {view ->
            view.findNavController().navigate(R.id.action_zeroFragment2_to_oneFragment2)
        }
        return binding.root
    }

}